program situacaoAluno;
uses crt;

var
  nota1, nota2, media: real;
  nome, situacao: string;

begin
  clrscr;
  write('Nome do aluno: ');
  readln(nome);
  write('Nota 1: ');
  readln(nota1);
  write('Nota 2: ');
  readln(nota2);

  media := (nota1 + nota2) / 2;

  if(media > 5)then
    if(media > 7)then
      situacao := 'APROVADO'
    else
      situacao := 'RECUPERACAO'
  else
    situacao := 'REPROVADO';

  writeln('Situacao do aluno ', nome,
          ' com media ', media:4:2, ':  ', situacao);

  readkey;
end.