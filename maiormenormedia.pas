program maiorMenorMedia;

uses crt;

var
  maior, menor, numero, soma,  i: integer;
  media: real;

begin
  clrscr;
  write('Numero 1: ');
  readln(numero);
  soma := numero;
  maior := numero;
  menor := numero;

  for i:=2 to 10 do
    begin
      write('Numero ',i,': ');
      readln(numero);
      soma := soma + numero;

      if(numero > maior)then
        maior := numero;

      if(numero < menor)then
        menor := numero;
    end;

    media := soma / 10;

    writeln('=====================');
    writeln('MAIOR Numero: ', maior);
    writeln('MENOR Numero: ', menor);
    writeln('MEDIA dos Numeros: ', media:4:2);

end.