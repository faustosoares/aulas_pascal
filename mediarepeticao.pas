program mediaRepeticao;
uses crt;

var
  num, soma, media: real;
  cont: integer;

begin
  clrscr;
  write('Informe um numero (-1 para sair): ');
  readln(num);
  soma := 0;
  cont := 0;

  while (num <> -1) do
    begin
        soma := soma + num;
        cont := cont + 1;
        write('Informe um numero (-1 para sair): ');
        readln(num);
    end;

  if not(cont = 0) then
    begin
      media := soma / cont;
      writeln('A media dos ',cont, ' numeros inserido e: ', media:4:2);
    end
  else
    writeln('Usuario informou -1 logo na entrada. Saindo do programa.');

end.

