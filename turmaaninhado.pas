program turmaAninhado;

uses crt;

var
  nome: string;
  nota, media, soma: real;
  i, j: integer;

begin
  clrscr;

  for i:=1 to 3 do
   begin
    write('Informe o nome do aluno: ');
    readln(nome);
    soma := 0;
    for j:=1 to 2 do
      begin
        write('Nota ' ,j, ': ');
        readln(nota);
        soma := soma + nota;
      end;
     media := soma / 2;
     writeln('###############');
     writeln('ALUNO: ', nome);
     writeln('MEDIA: ', media:4:2);
     writeln('###############');
   end;

end.
