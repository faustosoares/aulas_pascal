program opcaoMenu;

uses crt;

var
  opcao: integer;

begin
  clrscr;

  repeat
    writeln('Informe a opcao que deseja acessar no sistema: ');
    writeln('1 - Saldo');
    writeln('2 - Extrato');
    writeln('3 - Saque');
    writeln('4 - Transferencia');
    writeln('5 - Sair');
    write('Digite a opcao: ');
    readln(opcao);

    case opcao of
      1: writeln('Operacao de saldo selecionada');
      2: writeln('Operacao de extrato selecionada');
      3: writeln('Operacao de saque selecionada');
      4: writeln('Operacao de transferencia selecionada');
      5: writeln('Saindo do sistema');
    end;

  until opcao = 5;

end.
