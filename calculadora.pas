program calculadora;
uses crt;

var
  num1, num2, resultado: real;
  operador: char;

begin
  clrscr;
  write('Numero 1: ');
  readln(num1);
  write('Operador: ');
  readln(operador);
  write('Numero 2: ');
  readln(num2);

  case operador of
    '+': resultado := num1 + num2;
    '-': resultado := num1 - num2;
    '*': resultado := num1 * num2;
    '/': resultado := num1 / num2;
  end;

  writeln('O resultado da operacao e : ',resultado:4:2);

end.
