program fatorial;

uses crt;

var
  i, n, fat: integer;

begin
  clrscr;
  write('Informe o valor de n: ');
  readln(n);
  fat := 1;

  if(n = 0)then
   fat := 1
  else
    for i:=2 to n do
      fat := fat * i;

  writeln('O fatorial de ',n,' e: ', fat);
end.