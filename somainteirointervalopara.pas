program somaInteiroIntervaloPara;

uses crt;

var
 i, soma: integer;

begin
  clrscr;

  for i:=13 to 50 do
    soma := soma + i;

  writeln('A soma dos inteiros no intervalo [13,50] e: ', soma);
end.